FROM varnish
COPY default.vcl /etc/varnish/default.vcl
COPY launch-varnish.sh /

ENTRYPOINT /launch-varnish.sh