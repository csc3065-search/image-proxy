#!/bin/sh

if [ -z ${SECONDARY_BACKEND+x} ]; then
  SECONDARY_BACKEND=$DEFAULT_BACKEND
fi

echo "\"$DEFAULT_BACKEND\";" > /etc/varnish/default-backend
echo "\"$SECONDARY_BACKEND\";" > /etc/varnish/secondary-backend

varnishd -F -f /etc/varnish/default.vcl