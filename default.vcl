vcl 4.0;

import std;

backend default {
    .host = include "/etc/varnish/default-backend";
}

backend secondary {
    .host = include "/etc/varnish/secondary-backend";
}

sub vcl_backend_response {
    set beresp.ttl = 1w;
}

sub vcl_backend_fetch {
    set bereq.http.Host = "csc3065-image-store.s3-eu-west-1.amazonaws.com";
}

sub vcl_recv {
    if (req.url ~ "favicon.ico") {
        return(synth(404, "No favicon here."));
    }

    if (req.url ~ "healthz") {
        return(synth(200, "UP"));
    }


    set req.backend_hint = default;

    if (!std.healthy(req.backend_hint)) {
        set req.backend_hint = secondary;
        set req.http.X-Backend-Id = "secondary";
    } else {
        set req.backend_hint = default;
        set req.http.X-Backend-Id = "primary";
    }
}

sub vcl_deliver {
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
        set resp.http.X-Cache-Hits = obj.hits;
        set resp.http.X-Cache-Age = obj.age;
        set resp.http.X-Cache-Saved = obj.hits * std.integer(resp.http.Content-Length);
    } else {
        set resp.http.X-Cache = "MISS";
    }

    set resp.http.X-Backend-Id = req.http.X-Backend-Id;
}